package com.ortnec.task.steps.jsonplaceholder;

import java.util.List;
import java.util.Objects;
import java.util.Set;

import com.networknt.schema.ValidationMessage;
import com.ortnec.task.jsonplaceholder.webservices.api.JsonPlaceholderRestClient;
import com.ortnec.task.jsonplaceholder.webservices.datatransferobjects.UserDto;
import com.ortnec.task.utils.JsonUtil;

import net.thucydides.core.annotations.Step;

import lombok.AccessLevel;
import lombok.Getter;

public class JsonPlaceholderSteps {

    @Getter(AccessLevel.PRIVATE)
    private String usersResponseBody;

    private synchronized String getUsersResponseBody(final String url) {
        if (Objects.isNull(usersResponseBody)) {
            usersResponseBody = new JsonPlaceholderRestClient().getResponseBody(url);
        }
        return usersResponseBody;
    }

    @Step
    public List<UserDto> getJsonPlaceholderUsers(final String url) {
        return JsonUtil.fromJsonList(getUsersResponseBody(url), UserDto.class);
    }

    @Step
    public Set<ValidationMessage> getValidationErrorMessages(final String jsonSchema) {
        return JsonUtil.validateJsonBySchema(getUsersResponseBody(), jsonSchema);
    }

}
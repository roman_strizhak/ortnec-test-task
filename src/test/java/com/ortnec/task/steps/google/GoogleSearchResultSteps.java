package com.ortnec.task.steps.google;

import java.util.HashMap;
import java.util.Map;
import java.util.stream.IntStream;

import com.ortnec.task.google.pages.GoogleSearchResultPage;
import com.ortnec.task.utils.WebDriverUtil;

import net.serenitybdd.core.pages.WebElementFacade;
import net.thucydides.core.annotations.At;
import net.thucydides.core.annotations.Step;

public class GoogleSearchResultSteps {

    private GoogleSearchResultPage googleSearchResultPage;

    @Step
    public boolean isAtGoogleSearchResultPage() {
        final String pageRegexPattern = GoogleSearchResultPage.class.getAnnotation(At.class).value();
        return googleSearchResultPage.getDriver().getCurrentUrl().matches(pageRegexPattern);
    }

    @Step
    public String getFirstResultLinkValue() {
        return getPageTitleLink(0).getAttribute("href");
    }

    @Step
    public Map<String, Boolean> getActualResultMapForFoundPages(final String text) {
        final int foundItemsSize = googleSearchResultPage.getGoogleSearchResultItemWidgetList().size();
        final Map<String, Boolean> resultMap = new HashMap<>();
        IntStream.range(0, foundItemsSize).forEach(i -> {
            getPageTitleLink(i).click();
            WebDriverUtil.waitForPageLoad();
            final String pageUrl = WebDriverUtil.getDriver().getCurrentUrl();
            final boolean result = WebDriverUtil.getDriver().getPageSource().contains(text);
            resultMap.put(pageUrl, result);
            WebDriverUtil.getDriver().navigate().back();
            WebDriverUtil.waitForPageLoad();
        });
        return resultMap;
    }

    private WebElementFacade getPageTitleLink(final int index) {
        return googleSearchResultPage.getGoogleSearchResultItemWidgetList().get(index).getPageTitleLink();
    }
}
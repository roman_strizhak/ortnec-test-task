package com.ortnec.task.steps.google;

import com.ortnec.task.google.pages.GoogleSearchPage;

import net.thucydides.core.annotations.Step;

import org.openqa.selenium.Keys;

public class GoogleSearchSteps {

    private GoogleSearchPage googleSearchPage;

    @Step
    public void openGoogleSearchPage() {
        googleSearchPage.open();
    }

    @Step
    public void typeValueIntoSearchField(final String value) {
        googleSearchPage.getGoogleMainSearchWidget().getSearchInputField().type(value);
    }

    @Step
    public void pressEnterButton() {
        googleSearchPage.getGoogleMainSearchWidget().getSearchInputField().sendKeys(Keys.ENTER);
    }
}
package com.ortnec.task.definitions.jsonplaceholder;

import java.util.List;

import com.ortnec.task.jsonplaceholder.webservices.datatransferobjects.UserDto;
import com.ortnec.task.steps.jsonplaceholder.JsonPlaceholderSteps;

import net.thucydides.core.annotations.Steps;

import org.jbehave.core.annotations.Given;
import org.jbehave.core.annotations.Then;
import org.junit.Assert;

import static org.hamcrest.Matchers.empty;
import static org.hamcrest.Matchers.hasSize;

public class JsonPlaceholderDefinitions {

    @Steps
    private JsonPlaceholderSteps jsonPlaceholderSteps;

    private List<UserDto> jsonPlaceholderUsers;

    @Given("send request to '$url' url")
    public void sendRequest(final String url) {
        jsonPlaceholderUsers = jsonPlaceholderSteps.getJsonPlaceholderUsers(url);
    }

    @Then("JsonPlaceholder users response has '$objectsSize' user objects")
    public void checkUserObjects(final int objectsSize) {
        Assert.assertThat("Size is wrong", jsonPlaceholderUsers, hasSize(objectsSize));
    }

    @Then("JsonPlaceholder users response json is valid to '$jsonSchema' json schema")
    public void validateUsersJson(final String jsonSchema) {
        Assert.assertThat("Json validation is failed", jsonPlaceholderSteps.getValidationErrorMessages(jsonSchema),
                empty());
    }
}

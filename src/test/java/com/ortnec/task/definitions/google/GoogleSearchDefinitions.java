package com.ortnec.task.definitions.google;

import com.ortnec.task.steps.google.GoogleSearchSteps;

import net.thucydides.core.annotations.Steps;

import org.jbehave.core.annotations.Given;
import org.jbehave.core.annotations.When;

public class GoogleSearchDefinitions {

    @Steps
    private GoogleSearchSteps googleSearchSteps;

    @Given("user opens Google Search page")
    public void openGoogleSearchPage() {
        googleSearchSteps.openGoogleSearchPage();
    }

    @When("user types '$value' value into Search input field at Google Search page")
    public void typeValueIntoSearchField(final String value) {
        googleSearchSteps.typeValueIntoSearchField(value);
    }

    @When("user presses Enter button at Google Search page")
    public void pressEnterButton() {
        googleSearchSteps.pressEnterButton();
    }
}
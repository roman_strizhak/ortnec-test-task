package com.ortnec.task.definitions.google;

import java.util.Map;

import com.ortnec.task.steps.google.GoogleSearchResultSteps;

import net.thucydides.core.annotations.Steps;

import org.jbehave.core.annotations.Then;
import org.junit.Assert;

import static org.hamcrest.Matchers.is;

public class GoogleSearchResultsDefinitions {

    @Steps
    private GoogleSearchResultSteps googleSearchResultSteps;

    @Then("user is at Google Search Result page")
    public void isAtGoogleSearchResultPage() {
        Assert.assertTrue("User isn't at Google Search Result page",
                googleSearchResultSteps.isAtGoogleSearchResultPage());
    }

    @Then("user sees '$link' link is the first in result list at Google Search Result page")
    public void checkFirstLinkInSearchResults(final String link) {
        Assert.assertThat("First link is wrong", googleSearchResultSteps.getFirstResultLinkValue(), is(link));
    }

    @Then("user clicks on each page link from Search Result list and sees '$text' text")
    public void checkTextAtEachPage(final String text) {
        final Map<String, Boolean> actualResultMap = googleSearchResultSteps.getActualResultMapForFoundPages(text);
        final String errorMessage = String.format("Some pages don't contain '%s' text. Details: %s", text,
                actualResultMap.toString().replace(",", "\n"));
        actualResultMap.forEach((key, value) -> Assert.assertTrue(errorMessage, value));
    }
}
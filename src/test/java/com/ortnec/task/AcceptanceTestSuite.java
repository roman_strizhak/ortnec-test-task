package com.ortnec.task;

import com.ortnec.task.utils.DriversPathUtil;

import net.serenitybdd.jbehave.SerenityStories;

public class AcceptanceTestSuite extends SerenityStories {

    public AcceptanceTestSuite() {
        DriversPathUtil.setDriverPaths();
    }
}

Meta:
@application:jsonplaceholder

Scenario: 02 - send GET request and check response json
Meta:
@api_test
Given send request to 'https://jsonplaceholder.typicode.com/users' url
Then JsonPlaceholder users response has '10' user objects
And JsonPlaceholder users response json is valid to 'users-schema.json' json schema

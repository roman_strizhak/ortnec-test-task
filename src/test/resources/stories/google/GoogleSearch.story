Meta:
@application:google

Narrative:
In order to find Ortnec company
As an Google user
I want to search pages by text request

Scenario: 01 - Search the Ortnec company in Google service
Meta:
@ui_test
Given user opens Google Search page
When user types 'Ortnec' value into Search input field at Google Search page
And user presses Enter button at Google Search page
Then user is at Google Search Result page
And user sees 'http://ortnec.com/' link is the first in result list at Google Search Result page
And user clicks on each page link from Search Result list and sees 'ortnec' text
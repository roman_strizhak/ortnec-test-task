package com.ortnec.task.utils;

import java.util.Locale;
import java.util.Objects;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public final class OperatingSystemUtil {

    private static final String MAC_CHUNK = "mac";
    private static final String MAC_DARWIN_CHUNK = "darwin";
    private static final String WINDOWS_CHUNK = "win";
    private static final String LINUX_CHUNK = "nux";

    private static OSType osType;

    public enum OSType {
        WINDOWS,
        MACOS,
        LINUX,
        OTHER
    }

    public static OSType getOperatingSystemType() {
        if (Objects.isNull(osType)) {
            final String operationSystem = System.getProperty("os.name", "generic").toLowerCase(Locale.ENGLISH);
            if (operationSystem.contains(MAC_CHUNK) || operationSystem.contains(MAC_DARWIN_CHUNK)) {
                osType = OSType.MACOS;
            } else if (operationSystem.contains(WINDOWS_CHUNK)) {
                osType = OSType.WINDOWS;
            } else if (operationSystem.contains(LINUX_CHUNK)) {
                osType = OSType.LINUX;
            } else {
                osType = OSType.OTHER;
            }
        }
        return osType;
    }
}

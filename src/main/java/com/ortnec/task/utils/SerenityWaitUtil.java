package com.ortnec.task.utils;

import java.util.concurrent.TimeUnit;
import java.util.function.Supplier;

import net.serenitybdd.core.environment.ConfiguredEnvironment;
import net.thucydides.core.scheduling.NormalFluentWait;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public final class SerenityWaitUtil {

    @SuppressWarnings("unchecked")
    public static boolean waitUntil(final Supplier<Boolean> condition, final long timeoutMs, final long retryMs) {
        return (Boolean) new NormalFluentWait(condition).withTimeout(timeoutMs, TimeUnit.MILLISECONDS)
                .pollingEvery(retryMs, TimeUnit.MILLISECONDS).until(magic -> condition.get());
    }

    public static boolean waitUntil(final Supplier<Boolean> condition) {
        final long waitForTimeout = Long.parseLong(ConfiguredEnvironment.getConfiguration().getEnvironmentVariables()
                .getProperty("webdriver.wait.for.timeout"));
        return waitUntil(condition, waitForTimeout, 1000);
    }

}
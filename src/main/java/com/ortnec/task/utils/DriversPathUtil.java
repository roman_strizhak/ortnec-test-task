package com.ortnec.task.utils;

import com.ortnec.task.core.logging.Logger;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public final class DriversPathUtil {

    private static final String WEBDRIVER_CHROME_DRIVER = "webdriver.chrome.driver";
    private static final String MAC_CHROME_DRIVER_PATH = "drivers/osx/chromedriver";
    private static final String WINDOWS_CHROME_DRIVER_PATH = "drivers/windows/chromedriver.exe";
    private static final String LINUX_CHROME_DRIVER_PATH = "drivers/linux/64bit/chromedriver";

    public static void setDriverPaths() {
        switch (OperatingSystemUtil.getOperatingSystemType()) {
        case WINDOWS:
            setChromeDriverPath(WINDOWS_CHROME_DRIVER_PATH);
            break;
        case MACOS:
            setChromeDriverPath(MAC_CHROME_DRIVER_PATH);
            break;
        case LINUX:
            setChromeDriverPath(LINUX_CHROME_DRIVER_PATH);
            break;
        default:
            throw new IllegalArgumentException("Undefined Operating system");
        }
    }

    private static void setChromeDriverPath(final String pathToDriver) {
        System.setProperty(WEBDRIVER_CHROME_DRIVER, pathToDriver);
        Logger.out.info(String.format("%s path is set to %s", WEBDRIVER_CHROME_DRIVER, pathToDriver));
    }
}
package com.ortnec.task.utils;

import net.serenitybdd.core.Serenity;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;

import static com.ortnec.task.utils.SerenityWaitUtil.waitUntil;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public final class WebDriverUtil {

    public static WebDriver getDriver() {
        return Serenity.getWebdriverManager().getWebdriver();
    }

    public static JavascriptExecutor getJavascriptExecutor() {
        return (JavascriptExecutor) getDriver();
    }

    public static void waitForPageLoad() {
        waitUntil(() -> "complete".equals(getJavascriptExecutor().executeScript("return document.readyState")));
    }
}
package com.ortnec.task.utils;

import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.ParameterizedType;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.internal.$Gson$Types;
import com.networknt.schema.JsonSchema;
import com.networknt.schema.JsonSchemaFactory;
import com.networknt.schema.ValidationMessage;
import com.ortnec.task.core.logging.Logger;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public final class JsonUtil {

    private static final String JSON_SCHEMES_PATH = "/json/schemes/";

    public static <T> List<T> fromJsonList(final String json, final Class<T> type) {
        return getGson().fromJson(json, getParameterizedListType(type));
    }

    public static Set<ValidationMessage> validateJsonBySchema(final String json, final String jsonSchema) {
        final InputStream inputStream = JsonUtil.class.getResourceAsStream(JSON_SCHEMES_PATH + jsonSchema);
        final JsonSchema schema = getJsonSchemaFromStringContent(inputStream);
        final JsonNode node = getJsonNodeFromStringContent(json);
        return schema.validate(node);
    }

    private static JsonSchema getJsonSchemaFromStringContent(final InputStream jsonSchema) {
        return new JsonSchemaFactory().getSchema(jsonSchema);
    }

    private static JsonNode getJsonNodeFromStringContent(final String json) {
        JsonNode jsonNode = null;
        try {
            jsonNode = new ObjectMapper().readTree(json);
        } catch (IOException e) {
            Logger.out.warn(e);
        }
        return jsonNode;
    }

    private static Gson getGson() {
        return new GsonBuilder().create();
    }

    private static <T> ParameterizedType getParameterizedListType(final Class<T> type) {
        return $Gson$Types.newParameterizedTypeWithOwner(null, ArrayList.class, type);
    }

}
package com.ortnec.task.jsonplaceholder.webservices.api;

import com.ortnec.task.core.webservices.BaseWebservice;

import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;

public class JsonPlaceholderRestClient extends BaseWebservice {

    public String getResponseBody(final String httpUrl) {
        final HttpEntity<?> entity = new HttpEntity<>(getHeadersWithJson());
        final HttpEntity<String> response = sendAndGetResponse(httpUrl, HttpMethod.GET, entity, HttpStatus.OK);
        return response.getBody();
    }
}
package com.ortnec.task.google.widgets;

import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.core.pages.WebElementFacade;
import net.serenitybdd.core.pages.WidgetObjectImpl;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.pagefactory.ElementLocator;

import lombok.Getter;

@Getter
public class GoogleSearchResultItemWidgetImpl extends WidgetObjectImpl implements GoogleSearchResultItemWidget {

    @FindBy(xpath = ".//*[@class='rc']/*[@class='r']/a")
    private WebElementFacade pageTitleLink;

    @FindBy(xpath = "//*[@class='rc']/*[@class='s']//cite")
    private WebElementFacade shortPageLink;

    @FindBy(xpath = ".//*[@class='rc']/*[@class='s']//*[@class='st']")
    private WebElementFacade contentPart;

    public GoogleSearchResultItemWidgetImpl(final PageObject page, final ElementLocator locator,
            final WebElement webElement, final long timeoutInMilliseconds) {
        super(page, locator, webElement, timeoutInMilliseconds);
    }

    public GoogleSearchResultItemWidgetImpl(final PageObject page, final ElementLocator locator,
            final long timeoutInMilliseconds) {
        super(page, locator, timeoutInMilliseconds);
    }
}

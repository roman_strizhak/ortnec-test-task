package com.ortnec.task.google.widgets;

import net.serenitybdd.core.annotations.ImplementedBy;
import net.serenitybdd.core.pages.WebElementFacade;
import net.serenitybdd.core.pages.WidgetObject;

@ImplementedBy(GoogleMainSearchWidgetImpl.class)
public interface GoogleMainSearchWidget extends WidgetObject {

    WebElementFacade getSearchInputField();

    WebElementFacade getGoogleSearchButton();

    WebElementFacade getImFeelingLuckyButton();

}
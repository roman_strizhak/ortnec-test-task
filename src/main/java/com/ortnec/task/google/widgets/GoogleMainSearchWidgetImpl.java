package com.ortnec.task.google.widgets;

import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.core.pages.WebElementFacade;
import net.serenitybdd.core.pages.WidgetObjectImpl;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.pagefactory.ElementLocator;

import lombok.Getter;

@Getter
public class GoogleMainSearchWidgetImpl extends WidgetObjectImpl implements GoogleMainSearchWidget {

    @FindBy(id = "lst-ib")
    private WebElementFacade searchInputField;

    @FindBy(name = "btnK")
    private WebElementFacade googleSearchButton;

    @FindBy(name = "btnI")
    private WebElementFacade imFeelingLuckyButton;

    public GoogleMainSearchWidgetImpl(final PageObject page, final ElementLocator locator, final WebElement webElement,
            final long timeoutInMilliseconds) {
        super(page, locator, webElement, timeoutInMilliseconds);
    }

    public GoogleMainSearchWidgetImpl(final PageObject page, final ElementLocator locator,
            final long timeoutInMilliseconds) {
        super(page, locator, timeoutInMilliseconds);
    }
}

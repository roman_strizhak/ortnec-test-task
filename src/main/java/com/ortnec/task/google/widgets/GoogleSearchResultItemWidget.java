package com.ortnec.task.google.widgets;

import net.serenitybdd.core.annotations.ImplementedBy;
import net.serenitybdd.core.pages.WebElementFacade;
import net.serenitybdd.core.pages.WidgetObject;

@ImplementedBy(GoogleSearchResultItemWidgetImpl.class)
public interface GoogleSearchResultItemWidget extends WidgetObject {

    WebElementFacade getPageTitleLink();

    WebElementFacade getShortPageLink();

    WebElementFacade getContentPart();

}
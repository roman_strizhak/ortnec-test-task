package com.ortnec.task.google.pages;

import java.util.List;

import com.ortnec.task.google.widgets.GoogleSearchResultItemWidget;

import net.serenitybdd.core.annotations.findby.FindBy;
import net.thucydides.core.annotations.At;
import net.thucydides.core.annotations.DefaultUrl;
import net.thucydides.core.pages.PageObject;

import lombok.Getter;

@Getter
@DefaultUrl("https://www.google.com.ua/search")
@At("https://www.google.com.ua/search?.*")
public class GoogleSearchResultPage extends PageObject {

    @FindBy(css = ".med .srg > .g")
    private List<GoogleSearchResultItemWidget> googleSearchResultItemWidgetList;

}
package com.ortnec.task.google.pages;

import com.ortnec.task.google.widgets.GoogleMainSearchWidget;

import net.serenitybdd.core.annotations.findby.FindBy;
import net.thucydides.core.annotations.DefaultUrl;
import net.thucydides.core.pages.PageObject;

import lombok.Getter;

@Getter
@DefaultUrl("https://www.google.com")
public class GoogleSearchPage extends PageObject {

    @FindBy(id = "tsf")
    private GoogleMainSearchWidget googleMainSearchWidget;

}
package com.ortnec.task.core.logging;

import org.slf4j.LoggerFactory;

import lombok.AccessLevel;
import lombok.Getter;

public class Logger {

    private static final String BRACES = "{}";

    public static Logger out = new Logger(Logger.class.getName());

    @Getter(AccessLevel.PRIVATE)
    private final org.slf4j.Logger loggerClass;

    private Logger(final String loggerName) {
        this.loggerClass = LoggerFactory.getLogger(loggerName);
    }

    public void trace(final Object message) {
        getLoggerClass().trace(BRACES, message);
    }

    public void debug(final Object message) {
        getLoggerClass().debug(BRACES, message);
    }

    public void info(final Object message) {
        getLoggerClass().info(BRACES, message);
    }

    public void warn(final Object message) {
        getLoggerClass().warn(BRACES, message);
    }

    public void error(final Object message) {
        getLoggerClass().error(BRACES, message);
    }
}
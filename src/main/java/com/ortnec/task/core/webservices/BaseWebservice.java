package com.ortnec.task.core.webservices;

import java.util.Collections;
import java.util.Objects;

import com.ortnec.task.core.logging.Logger;

import org.assertj.core.api.Assertions;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.http.client.ClientHttpRequestFactory;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.HttpServerErrorException;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

public class BaseWebservice {

    private static final String NEW_LINE = System.getProperty("line.separator");
    private static final String REQUEST_BODY_PREFIX = "Request body: ";
    private static final String RESPONSE_BODY_PREFIX = "Response body: ";
    private static final String URL_DESIGNATOR = "URL: ";

    private static RestTemplate restTemplate;
    private static HttpHeaders HEADERS = new HttpHeaders();

    protected static HttpHeaders getHeadersWithJson() {
        HEADERS.setAccept(Collections.singletonList(MediaType.APPLICATION_JSON));
        HEADERS.setContentType(MediaType.APPLICATION_JSON_UTF8);
        return HEADERS;
    }

    private static synchronized RestTemplate getRestTemplate() {
        if (Objects.isNull(restTemplate)) {
            restTemplate = new RestTemplate();
            restTemplate.setRequestFactory(createHttpClientFactory());
        }
        return restTemplate;
    }

    protected static ResponseEntity<String> sendAndGetResponse(final String httpUrl, final HttpMethod httpMethod,
            final HttpEntity<?> entity, final HttpStatus code) {
        var uri = UriComponentsBuilder.fromHttpUrl(httpUrl).build().toUri();
        ResponseEntity<String> response;
        var request = httpMethod + " " + uri + NEW_LINE + REQUEST_BODY_PREFIX
                + entity.getBody();
        Logger.out.info(request);
        try {
            response = getRestTemplate().exchange(uri, httpMethod, entity, String.class);
            var responseLog = response.getStatusCode() + " " + uri + NEW_LINE
                    + RESPONSE_BODY_PREFIX + response.getBody();
            Logger.out.info(responseLog);
        } catch (final HttpClientErrorException | HttpServerErrorException e) {
            Logger.out.error(e);
            var statusText = e.getStatusText() + NEW_LINE + URL_DESIGNATOR + uri.toString() + NEW_LINE
                    + e.getResponseBodyAsString();
            throw new HttpClientErrorException(e.getStatusCode(), statusText);
        }
        Assertions.assertThat(response.getStatusCode()).as("Status code").isEqualTo(code);
        return response;
    }

    private static ClientHttpRequestFactory createHttpClientFactory() {
        var timeout = 10000;
        var clientFactory = new HttpComponentsClientHttpRequestFactory();
        clientFactory.setConnectionRequestTimeout(timeout);
        clientFactory.setReadTimeout(timeout);
        clientFactory.setConnectTimeout(timeout);
        return clientFactory;
    }

}
### Required software
To install CTCO-FPOS Test Automation Framework (TAF) the following software is required:

1. [Java JDK 11]()

2. [Apache Maven 3.3.9 or above](https://maven.apache.org/download.cgi)

3. [Git](https://git-scm.com/downloads)

### Working with framework

#### Download project
```
$ git clone https://roman_strizhak@bitbucket.org/roman_strizhak/ortnec-test-task.git
OR
$ git clone git@bitbucket.org:roman_strizhak/ortnec-test-task.git
```

#### Run all tests
```
$ mvn integration-test
```

#### Run tests by @metafilter - example:
```
$ mvn integration-test -Dmetafilter="+api_test"
```

#### Generating Serenity BDD report
```
$ mvn serenity:aggregate
```
Generated report you can find at file: \target\site\serenity\index.html